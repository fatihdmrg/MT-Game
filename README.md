# MT-Game
Mathematic 4 transactions Android Application

Kendinizi 4 işlem konusunda geliştireceğiniz ve pratik kazanacağınız bir uygulamadır.Her doğru cevapladığınız soru için puan kazanacağınız bu uygulama ile matematiğinizi geliştirebilir ve derslerinize katkı sağlayabilirsiniz.

You will improve your 4 processes and you will gain practice. You will earn points for the question you answered correctly, with this application you can develop your math and contribute to your lessons.

![login](https://i0.wp.com/fatihdemirag.net/wp-content/uploads/2018/01/Selection_018.png)

![mainpage](https://i0.wp.com/fatihdemirag.net/wp-content/uploads/2018/01/Selection_019.png)

![gameover](https://i1.wp.com/fatihdemirag.net/wp-content/uploads/2018/01/Selection_021.png)
