package fatihdemirag.net.mtgame;//Developer FXD-FATİH DEMİRAĞ

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;


public class MainActivity extends AppCompatActivity {

    Button baslaButton;
    TextView yardimButton;
    Intent intent;
    TextView load,baslik;

    public void yardimUyari()
    {
        AlertDialog.Builder uyari=new AlertDialog.Builder(this);
        uyari.setMessage(R.string.kurallar);
        uyari.setTitle(R.string.kurallar_baslik);
        uyari.setPositiveButton(R.string.tamam, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        AlertDialog alert=uyari.create();
        alert.show();
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder cikis=new AlertDialog.Builder(this);
        cikis.setMessage(R.string.giris_uyari_mesaj);
        cikis.setTitle(R.string.giris_uyari_baslik);
        cikis.setPositiveButton(R.string.evet, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });
        cikis.setNegativeButton(R.string.hayir  , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog=cikis.create();
        alertDialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        load=(TextView)findViewById(R.id.load);
        baslaButton=(Button)findViewById(R.id.button);
        baslaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load.setVisibility(View.VISIBLE);
                intent=new Intent(MainActivity.this,Baslangic.class);
                startActivity(intent);
            }
        });
        baslik=(TextView)findViewById(R.id.baslik);
        baslik.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,android.R.anim.fade_in));
        yardimButton=(TextView)findViewById(R.id.yardimButton);
        yardimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yardimUyari();
            }
        });
    }
}